#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import (
    collatz_read,
    collatz_eval,
    collatz_print,
    collatz_solve,
    get_floor_multiple,
    get_ceil_multiple,
    get_cached_ranges,
    get_ordered_begin_end,
    collatz_cycle_length,
    get_cached_collatz_max,
    collatz_meta_eval,
)

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )

    # -----
    # MY UNIT TESTS
    # -----

    def test_get_floor_multiple(self):
        v = 2433
        floor_multiple = get_floor_multiple(v)
        self.assertEqual(floor_multiple, 2000)

    def test_ceil_multiple(self):
        v = 3544
        floor_multiple = get_ceil_multiple(v)
        self.assertEqual(floor_multiple, 4000)

    def test_get_cached_ranges(self):
        beg_key, end_key = get_cached_ranges(1500, 3500)
        self.assertEqual(beg_key, 2001)
        self.assertEqual(end_key, 3001)

    def test_get_ordered_begin_end(self):
        i = 7023
        j = 443
        begin, end = get_ordered_begin_end(i, j)
        self.assertEqual(begin, 443)
        self.assertEqual(end, 7023)

    def test_collatz_cycle_length(self):
        v = 10
        cycle_length = collatz_cycle_length(v)
        self.assertEqual(cycle_length, 7)

    def test_collatz_meta_eval(self):
        begin = 1500
        end = 3500
        max = collatz_meta_eval(begin, end)
        self.assertEqual(max, 217)

    def test_get_cached_collatz_max(self):
        cached_begin = 1001
        cached_end = 3001
        max_of_range = get_cached_collatz_max(cached_begin, cached_end)
        self.assertEqual(max_of_range, 217)

    def test_meta_solve(self):
        r = StringIO("1500 3500")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "1500 3500 217\n")

    def test_meta_large_range_solve(self):
        r = StringIO("22033 50050")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "22033 50050 324\n")

    def test_reversed_input(self):
        r = StringIO("3500 1500")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "3500 1500 217\n")

    def test_same_number_solve(self):
        r = StringIO("10 10")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "10 10 7\n")


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
