# CS373: Software Engineering Collatz Repo

* Name: Sriram Hariharan

* EID: sgh874

* GitLab ID: sghsri

* HackerRank ID: @10001shh

* Git SHA: 222f66e16bfa083c6f8a5e61610e65e827203291

* GitLab Pipelines: https://gitlab.com/sghsri/cs373-collatz/pipelines

* Estimated completion time: 5 hours

* Actual completion time: 12 hours

* Comments: The actual algorithm wasn't difficult, but i initially overlooked some edge cases which wasted some of my time in
trying to debug and find it. I enjoyed going through a structured software development process.
